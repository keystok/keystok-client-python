# Makefile for building the distribution package in dist
all:
	python setup.py sdist

upload:
	python setup.py sdist upload -r pypi

uploadtest:
	python setup.py sdist upload -r pypi-test

